﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundMovement : MonoBehaviour {

    public float speed;
    float startPosition;

	// Use this for initialization
	void Start () {
        startPosition = transform.position.x;
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = new Vector2(transform.position.x - (speed / 100), transform.position.y);
        if(transform.localPosition.x <= -8) 
        {
            transform.position = new Vector2(startPosition, transform.position.y);
        }
	}
}
