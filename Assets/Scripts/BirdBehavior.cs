﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdBehavior : MonoBehaviour {

    public Vector2 velocity;
    float initialGravity;
    Rigidbody2D rb;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();
        initialGravity = rb.gravityScale;
        rb.gravityScale = 0;
	}
	
	// Update is called once per frame
	void Update () {
        
	}

	void FixedUpdate ()
	{
		//Detect when the Return key is pressed down
        if (Input.GetKeyDown(KeyCode.Return))
        {
            rb.gravityScale = initialGravity;
            rb.velocity = velocity;
        }
	}
}
